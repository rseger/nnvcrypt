import numpy
import pickle
import random

def genPasswords(numPasswords=70000, numClasses=10):
	perClass = numPasswords//numClasses
	sizeShared = 500
	sizePass = 28*28
	passwords = []
	for i in range(numClasses):
		password = bytes([random.randint(0, 1) for p in range(sizeShared)])
		#password = [0]*sizeShared
		for j in range(perClass):
			tmp = [random.randint(0, 255) for p in range(sizePass)]
			offset = random.randint(0, sizePass - sizeShared)
			for p in range(len(password)):
				tmp[offset+p] = password[p]
			passwords.append((tmp, i))
	random.shuffle(passwords)
	return passwords

if __name__ == "__main__":
	filename = "passwords.txt"
	outfile = open(filename, 'wb')
	passwords = genPasswords()
	pickle.dump(passwords, outfile)
	outfile.close()