from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
import matplotlib.pyplot as pyplot
from mpl_toolkits import mplot3d
import numpy
import random
import hashlib
import os
import pickle
from cryptography.fernet import Fernet

def hashTransform(dataList, hashName="md5", rand=False, enc=False):
    tmp = []
    key = Fernet.generate_key()
    f = Fernet(key)
    #key2 = get_random_bytes(16)
    #cipher = AES.new(key2, AES.MODE_CBC)
    size = len(dataList[0])
    for each in dataList:
        if not rand:
            if not enc:
                each = each.flatten()
                hasher = hashlib.new(hashName)
                hasher.update(each)
                tmp.append([float(i) for i in hasher.digest()])
            else:
                #info = each.flatten().tobytes()
                
                token = f.encrypt(bytes(each))
                t = []
                for i in range(len(token)):
                    t.append(float(token[i]))
                tmp.append(t[20:-32])
                
                """
                #ciphertext = cipher.encrypt(info)
                cipher = AES.new(key2, AES.MODE_EAX)
                ciphertext, _ = cipher.encrypt_and_digest(info)
                tmp.append([float(i) for i in ciphertext])
                """
        else:
            tmp.append(numpy.random.rand(20)*255)
    
    return numpy.array(tmp)

def hashNetwork(hashName="md5", rand=False, enc=False):
    batchSize = 128
    numClasses = 10
    epochs = 200
    netSize = 1000
    numExtra = 0
    img_rows, img_cols = 28, 28
    (trainingData, trainingCat), (testingData, testingCat) = mnist.load_data()
    trainingData = hashTransform(trainingData, hashName, rand, enc)
    testingData = hashTransform(testingData, hashName, rand, enc)
    testingData = numpy.array(testingData)
    trainingData = numpy.array(trainingData)

    trainingData /= 255
    testingData /= 255
    trainingCat = keras.utils.to_categorical(trainingCat, numClasses)
    testingCat = keras.utils.to_categorical(testingCat, numClasses)
    model = Sequential()
    model.add(Dense(netSize, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(netSize//2, activation='relu'))
    #model.add(Dropout(0.3))
    #model.add(Dense(netSize//4, activation='relu'))
    model.add(Dense(numClasses, activation='softmax'))
    model.compile(loss=keras.losses.categorical_crossentropy,
                optimizer=keras.optimizers.Adadelta(),
                metrics=['accuracy'])
    history = model.fit(trainingData, trainingCat,
            batch_size=batchSize,
            epochs=epochs,
            verbose=1,
            validation_data=(testingData, testingCat))
    score = model.evaluate(testingData, testingCat, verbose=0)
    #print('Test loss:', score[0])
    #print('Test accuracy:', score[1])
    return score[1], history

def explorePasswords(hashName='md5', enc=True, filename="passwords.txt"):
    infile = open(filename, 'rb')
    passwords = numpy.array(pickle.load(infile))
    infile.close()
    passwords = numpy.array(passwords)
    data = passwords[:, 0]
    classes = passwords[:, 1]
    sizeTest = 10000
    netSize = 1000
    epochs = 200
    numClasses = 10
    trainingData = data[sizeTest:]
    testingData = data[0:sizeTest]
    trainingCat = classes[sizeTest:]
    testingCat = classes[0:sizeTest]
    testingData = hashTransform(testingData, hashName=hashName, enc=enc)
    trainingData = hashTransform(trainingData, hashName=hashName, enc=enc)
    testingData /= 255
    trainingData /= 255

    testingCat = keras.utils.to_categorical(testingCat, numClasses)
    trainingCat = keras.utils.to_categorical(trainingCat, numClasses)
    
    model = Sequential()
    model.add(Dense(netSize, activation='relu'))
    model.add(Dense(netSize//2, activation='relu'))
    #model.add(Dense(netSize//4, activation='relu'))
    model.add(Dense(numClasses, activation='softmax'))
    model.compile(loss=keras.losses.categorical_crossentropy,
                optimizer=keras.optimizers.Adadelta(),
                metrics=['accuracy'])
    history = model.fit(trainingData, trainingCat,
            batch_size=128,
            epochs=epochs,
            verbose=1,
            validation_data=(testingData, testingCat))
    score = model.evaluate(testingData, testingCat, verbose=0)
    #print('Test loss:', score[0])
    #print('Test accuracy:', score[1])
    return score[1], history

if __name__ == "__main__":
    #os.environ["CUDA_VISIBLE_DEVICES"] = "-1" #small networks faster on CPU
    score, history = explorePasswords()
    #score, history = explorePasswords(filename="passwords.txt")
    #score, history = explorePasswords(filename="passwordsPicLen.txt")
    #score, history = explorePasswords(filename="passwordsShared0.txt")
    #score, history = explorePasswords(filename="passwordsShared01.txt")
    #score, history = explorePasswords(filename="passwordsShared0Pic.txt")
    #score, history = explorePasswords(filename="passwordsShared01Pic.txt")
    #score, history = hashNetwork(enc=True)
    pyplot.plot(history.history['accuracy'])
    pyplot.plot(history.history['val_accuracy'])
    pyplot.title('Derived Information')
    pyplot.ylabel('accuracy')
    pyplot.xlabel('epoch')
    pyplot.legend(['train', 'test'], loc='upper left')
    pyplot.show()